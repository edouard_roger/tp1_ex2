# -- coding: utf-8 --

"""
Created on Fri Mar 13 15:10:17 2020

@author: edouard.roger
"""


class SimpleCalculator:

    """ This class allows us to perform various operations """

    def __init__(self, var_a, var_b):
        self.var_a = var_a
        self.var_b = var_b
        self.result = 0

    def sum(self):

        """ Objective: Do a sum between 2 values

    Parameters:
        var_a: First variables for sum
        var_b: Second variables for sum

    Returns:
        result: The sum's result
        """

        result = self.var_a + self.var_b
        return result

    def subtract(self):

        """ Objective: Do a substraction between 2 values

    Parameters:
        var_a: First variables for substraction
        var_b: Second variables for substraction

    Returns:
        result: The dubstraction's result
        """

        result = self.var_a - self.var_b
        return result

    def multiply(self):

        """ Objective: Do a multiplication between 2 values

    Parameters:
        var_a: First variables for multiplication
        var_b: Second variables for multiplication

    Returns:
        result: The multiplication's result
        """

        result = self.var_a * self.var_b
        return result

    def divide(self):

        """ Objective: Do a division between 2 values

    Parameters:
        var_a: First variables for division
        var_b: Second variables for division

    Returns:
        result: The division's result
        """

        result = self.var_a / self.var_b
        return result


CALC = SimpleCalculator(10, 2)
SOMME = CALC.sum()
SOUSTRACTION = CALC.subtract()
MULTIPLICATION = CALC.multiply()
DIVISION = CALC.divide()

print(SOMME)
print(SOUSTRACTION)
print(MULTIPLICATION)
print(DIVISION)
